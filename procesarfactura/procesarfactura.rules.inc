<?php
/**
 * Implement hook_rules_action_info()
 * Declare any meta data about actions for Rules
 */

function procesarfactura_rules_action_info() {
  $actions = array(
    'procesar_factura_xml' => array(
        'label' => t('Procesar factura'),
        'group' => t('factura'),
        'parameter' => array(
            'file' => array(
                    'type' => 'file',
                    'label' => t('Archivo a procesar'),
                    ),
            'node' => array(
                'type' => 'node',
                'label' => t('Nodo a procesar'),
                ),
        ),
        /*
        'provides' => array(
          'factura_version' => array(
            'type' => 'text',
            'label' => 'version de factura',
          ),
        ),*/
      ),
    );
      return $actions;
} // fin de actions_info

// rules function procesar factura
function procesar_factura_xml($file, $node) {
  $node = node_load($node->nid);
  $nidflagged = $node->nid;
  //dpm($node);
  //dpm($file);

  // get $ur path to load $file
  $uri = $node->field_factura_xml['und'][0]['uri'];
  // Take a look at: file.inc::file_load_multiple
  $files = file_load_multiple(array(), array('uri' => $uri));
  $file = reset($files); // If empty, $file will be false, otherwise will contain the required file

 //drupal_set_message($uri, 'status', FALSE);
 //drupal_set_message($file->fid, 'status', FALSE);
 //drupal_set_message($node->nid, 'status', FALSE);

   //unset($xml);
   $xml = null;
   $xml = simplexml_load_file($uri);

//si no pudo subir la factura, mostrar error, unflag node
if(!$xml) {
  drupal_set_message('Su archivo XML no fue cargado correctamante, favor de intentar nuevamente.', 'status', FALSE);
  drupal_set_message('Nota: despues de subir el archivo al servidor, haga clic en el nombre de su archivo,
  si se muestra en pantalla, ya esta cargado correctamente, navege hacia atras su navegador y procese su factura.', 'status', FALSE);

  unflagg_node($nidflagged);

}

//SI HAY UN ARCHIVO XML
else {

  $ns = $xml->getNamespaces(true);
  $xml->registerXPathNamespace('c', $ns['cfdi']);
  $xml->registerXPathNamespace('t', $ns['tfd']);
  //$xml->registerXPathNamespace('p', $ns['pago10']);

  //leo el tipo de comprobante
  foreach ($xml->xpath('//cfdi:Comprobante') as $cfdiComprobante){
     $tipocomprobante =  $cfdiComprobante['TipoDeComprobante'];
     $tc = $tipocomprobante;

     //si no es version 3.3 EXIT
     $version =  $cfdiComprobante['Version'];


     //SWITCH TIPO DE COMPROBANTE, set title
     switch ($tipocomprobante) {
       case 'T':
         $tc = 'Traslado';
         break;
       case 'N':
         $tc = 'Nomina';
         break;
       case 'P':
         $tc = 'Pago';
         break;
       case 'E':
         $tc = 'Egreso';
         break;
       case 'I':
         $tc = 'Ingreso';
         break;

       default:
         $tc = 'NO DEFINIDO';
         break;
     }

     if ($tc) {
       $tc = $tipocomprobante .' → '.$tc;
     }

    }

    // ferifica CFDI version = 3.3
  if ($version == 3.3) {

    //SWITCH TIPO DE COMPROBANTE, procesar
    switch ($tipocomprobante) {
       case 'T': //case traslado
         drupal_set_message('El tipo de Comprobante que intenta procesar no es valido', 'error', FALSE);
         unflagg_node($nidflagged);
         break;

       case 'N': // case nomina
         drupal_set_message('El tipo de Comprobante que intenta procesar no es valido', 'error', FALSE);
         unflagg_node($nidflagged);
         break;

       case 'P': //case pago
       case 'E': //case egreso
       case 'I': //case ingreso
         //drupal_set_message('tipo: ' .$tipocomprobante, 'status', FALSE);

              //$version =  $cfdiComprobante['Version'];
              $fechaemision =  $cfdiComprobante['Fecha'];
              $total =  $cfdiComprobante['Total'];
              $subtotal =  $cfdiComprobante['SubTotal'];
              $formadepago =  $cfdiComprobante['FormaPago'];
              //$tipocomprobante =  $cfdiComprobante['TipoDeComprobante'];
              $numerocertificado = $cfdiComprobante['NoCertificado'];
              $metododepago =  $cfdiComprobante['MetodoPago'];
              $folio =  $cfdiComprobante['Folio'];
              $lugarexpedicion =  $cfdiComprobante['LugarExpedicion'];
              $moneda =  $cfdiComprobante['Moneda'];
              $serie =  $cfdiComprobante['Serie'];
              $condicionespago =  $cfdiComprobante['CondicionesDePago'];

              //forma de pago
              $fp = '';
              switch ($formadepago) {
                case '01':
                  $fp = 'Efectivo';
                  break;
                case '02':
                  $fp = 'Cheque nominativo';
                  break;
                case '03':
                  $fp = 'Trasferencia electrónica de fondos';
                  break;
                case '04':
                  $fp = 'Tarjeta de crédito';
                  break;
                case '05':
                  $fp = 'Monedero electrónico';
                  break;
                case '06':
                  $fp = 'Dinero electrónico';
                  break;
                case '08':
                  $fp = 'Vales de despensa';
                  break;
                case '12':
                  $fp = 'Dación de pago';
                  break;
                case '13':
                  $fp = 'Pago por subrogacion';
                  break;
                case '14':
                  $fp = 'Pago por consignación';
                  break;
                case '15':
                  $fp = 'Condonación';
                  break;
                case '17':
                  $fp = 'Compensación';
                  break;
                case '23':
                  $fp = 'Novación';
                  break;
                case '24':
                  $fp = 'Confución';
                  break;
                case '25':
                  $fp = 'Remisión de deuda';
                  break;
                case '26':
                  $fp = 'Prescripción o caducidad';
                  break;
                case '27':
                  $fp = 'A satisfación del acreedor';
                  break;
                case '28':
                  $fp = 'Tarjeta de débito';
                  break;
                case '29':
                  $fp = 'Tarjeta de servicios';
                  break;
                case '99':
                  $fp = 'Por definir';
                  break;

                default:
                  # code...
                  $fp = '';
                  break;
              }

              if ($fp) {
                $fp = $formadepago .' → '.$fp;
              }


              //metodo de pago
              $mp ='';
              switch ($metododepago) {
                case 'PUE':
                  $mp = 'Pago en una sola exibición';
                  break;
                case 'PPD':
                  $mp = 'Pago en parcialidades o diferido';
                  break;

                default:
                    $mp ='';
                  break;
              }

              if ($mp) {
                  $mp = $metododepago .' → '.$mp;
              }


              //drupal_set_message($fechaemision, 'status', FALSE);

              //set $node field COMROBANTE values
              $node->field_factura_version['und'][0]['value'] = $version;
              $node->field_factura_fecha_de_emision['und'][0]['value'] = $fechaemision;
              if ($total<>0) {
                $node->field_factura_total['und'][0]['value'] = $total;
              }
              if ($subtotal<>0) {
                $node->field_factura_subtotal['und'][0]['value'] = $subtotal;
              }
              if ($fp) {
                $node->field_factura_forma_de_pago['und'][0]['value'] = $fp;
              }
              if ($mp) {
                $node->field_factura_metodo_de_pago['und'][0]['value'] = $mp;
              }
              $node->field_fact_tipo_de_comprobante['und'][0]['value'] = $tc;
              $node->field_factu_num_de_certificado['und'][0]['value'] = $numerocertificado;
              if ($folio) {
                $node->field_factura_folio['und'][0]['value'] = $folio;
              }
              $node->field_factura_lugarexpedicion['und'][0]['value'] = $lugarexpedicion;
              if ($moneda) {
                $node->field_factura_moneda['und'][0]['value'] = $moneda;
              }
              if ($serie) {
                  $node->field_factura_serie['und'][0]['value'] = $serie;
              }
              if ($condicionespago) {
                $node->field_factura_condicionesdepago['und'][0]['value'] = $condicionespago;
              }

              // calculo de impuestos trasladados
              if ($total<>0) {
                $impuestostrasladados = $node->field_factura_total['und'][0]['value'] - $node->field_factura_subtotal['und'][0]['value'];
                $node->field_factura_impuestos_traslada['und'][0]['value'] = $impuestostrasladados;
              }


              foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Emisor') as $Emisor){

                $emisorrfc = $Emisor['Rfc'];
                $emisornombre =  $Emisor['Nombre'];
                //$usocfdi = $Emisor['UsoCFDI'];
                $regimenfiscal = $Emisor['RegimenFiscal'];

                $node->field_factura_rfc_emisor['und'][0]['value'] = $emisorrfc;
                $node->field_factura_emisor_nombre['und'][0]['value'] = $emisornombre;
                //$node->field_factura_uso_cfdi['und'][0]['value'] = $usocfdi;
                $node->field_fact_emisor_regimen_fiscal['und'][0]['value'] = $regimenfiscal;

              }

              foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Receptor') as $Receptor){
                $receptorrfc = $Receptor['Rfc'];
                $receptornombre =  $Receptor['Nombre'];
                $usocfdi = $Receptor['UsoCFDI'];

                // titulo del uso
                $uso = '';
                switch ($usocfdi) {
                  case 'G01':
                    $uso = 'Adquisición de mercancías';
                    break;
                  case 'G02':
                    $uso = 'Devoluciones, descuentos o bonificaciones';
                    break;
                  case 'G03':
                    $uso = 'Gastos en general';
                    break;
                  case 'I01':
                    $uso = 'Construcciones';
                    break;
                  case 'I02':
                    $uso = 'Mobilario y equipo de oficina por inversiones';
                    break;
                  case 'I03':
                    $uso = 'Equipo de transporte';
                    break;
                  case 'I04':
                    $uso = 'Equipo de computo y accesorios';
                    break;
                  case 'I05':
                    $uso = 'Dados, troqueles, moldes, matrices y herramental';
                    break;
                  case 'I06':
                    $uso = '	Comunicaciones telefónicas';
                    break;
                  case 'I07':
                    $uso = 'Comunicaciones satelitales';
                    break;
                  case 'I08':
                    $uso = 'Otra maquinaria y equipo';
                    break;
                  case 'G01':
                    $uso = 'Adquisición de mercancías';
                  break;
                  case 'D01':
                    $uso = 'Honorarios médicos, dentales y gastos hospitalarios.';
                    break;
                  case 'D02':
                    $uso = 'Gastos médicos por incapacidad o discapacidad';
                    break;
                  case 'D03':
                    $uso = 'Gastos funerales';
                    break;
                  case 'D04':
                    $uso = 'Donativos';
                    break;
                  case 'D05':
                    $uso = 'Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).';
                    break;
                  case 'D06':
                    $uso = 'Aportaciones voluntarias al SAR.	';
                    break;
                  case 'D07':
                    $uso = 'Primas por seguros de gastos médicos';
                    break;
                  case 'D08':
                    $uso = 'Gastos de transportación escolar obligatoria';
                    break;
                  case 'D09':
                    $uso = 'Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.';
                    break;
                  case 'D10':
                    $uso = 'Pagos por servicios educativos (colegiaturas)';
                    break;
                  case 'P01':
                    $uso = 'Por definir';
                    break;


                  default:
                      $uso = '';
                    break;

                }

                if ($uso) {
                    $uso = $usocfdi .' → '.$uso;
                }


                $node->field_factura_receptor_rfc['und'][0]['value'] = $receptorrfc;
                $node->field_factura_receptor_nombre['und'][0]['value'] = $receptornombre;
                $node->field_factura_uso_cfdi['und'][0]['value'] = $uso;

              }

                // verifica que sea P y obtiene el monto
                if($tipocomprobante == 'P') {
                  foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Complemento//pago10:Pagos//pago10:Pago') as $Pago10){
                    $montop = $Pago10['Monto'];
                    //drupal_set_message('monto: '.$montop, 'status', FALSE);
                    $node->field_factura_pago10_monto['und'][0]['value'] = $montop;
                    }

                  }


              node_save($node);

              unset_file_node();

         break;

      // el tipo de factura no existe
       default:
       drupal_set_message('El tipo de Comprobante que intenta procesar no es valido', 'error', FALSE);
       unflagg_node($nidflagged);
       unset_file_node();
         break;
       } //fin de procesar factura
     } // fin del IF no es version 3.3

     // el CFDI no es version 3.3
      else {
        drupal_set_message('El tipo de Comprobante que intenta procesar no es valido', 'error', FALSE);
        unflagg_node($nidflagged);
        unset_file_node();
      }
    } // fin del else, si habia xml

} // fin de funcion rules procesar_factura_xml

// function UNFLAG node
function unflagg_node($nidflagged){
  $flag = flag_get_flag('flag_nodo_factura_cliente');
    if ($flag) {
      // Flag node
      //$flag->flag('flag', $nidflagged);

      // Unflag node
      $flag->flag('unflag', $nidflagged);
    }
}

//function UNSET ftp_all
function unset_file_node(){
  unset($xml);
  unset($node);
  unset($file);

}
